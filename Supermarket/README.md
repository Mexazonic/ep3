# README

Ressalvas e Informações:

* Os commits recentes foram uma tentativa de evitar erros durante a avaliação do projeto, pois o banco de dados não estava sendo sincronizado adequadamente. Ao conferir se o projeto funcionava em outro computador, deparei-me com uma série de erros (referentes a não possuir categorias, usuários, administradores, produtos) que impossibilitariam a correção e até mesmo a visualização do trabalho. 

* A funcionalidade do Carrinho de Compras está a funcionar normalmente

* Em caso de erro de login, ou excesso de id referentes ao Order, limpar os cookies e/ou dados de navegação do browser

* O usuário administrador possui os seguintes dados: email: "goku@vegetta.dbz", senha: "bazinga73"

* O usuário comum pode ser cadastrado ou, se preferir, pode ser utilizado o seguinte usuário: email: "luffy@mokeyd.gomu", senha: "bazinga73"

* Houve a utilização das seguintes gems => Devise, searchkick, haml, paper-clip

* Para utilizar a searchkick (Realizar buscas dentre os produtos) é necessário usar o comando 'service elasticsearch start' no terminal, para execução desta funcionalidade. Pode-se utilizar http://localhost:9200/ no navegador para verificar se o comando foi de fato realizado

* Torna-se necessário a utilização do comando rake db:migrate para plena funcionalidade do código

* O sistema pode ser usado por um administrador padrão, para mais de um admin é necessário cadastrá-lo no banco de dados (@user.admin = true), de mesmo modo as categorias também deve ser acrescentadas pelo console do rails

* Utilização do Bootstrap para refinamento da Interface

* O sistema foi baseado em uma certa observação, muitos supermercados acabam oferecendo o serviço de compras a distância, entretanto esse sistema é ofertado via whatsapp, o que pode se tornar um pouco desgastante tanto para o cliente quanto para o funcionário, esse foi o direcionamento central do projeto


 
