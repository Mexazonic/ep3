class PagesController < ApplicationController
  def home
    if !current_user.nil?
      redirect_to products_path
    end
  end
end
