class ProductsController < ApplicationController
  before_action :find_product, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:show, :index]

  def search
    if params[:search].present?
      @products = Product.search(params[:search])
    else
      @products = Product.all
    end
  end

  def index

    if params[:category].blank?
      @products = Product.all
    else
      @category_id =Category.find_by(name: params[:category]).id
      @products = Product.where(category_id: @category_id)
    end

    @order_item = current_order.order_items.new
  end

  def show
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(products_params)
    #@product.user = current_user

    if @product.save
      redirect_to @product
    else
      render "New"
    end
  end

  def edit
  end

  def update
    if @product.update(products_params)
      redirect_to @product
    else
      render "Editar"
    end
  end

  def destroy
    @product.destroy
    redirect_to products_path
  end

  private

  def products_params
    params.require(:product).permit(:name, :price, :mark, :category_id, :image)
  end

  def find_product
    @product = Product.find(params[:id])
  end
end
