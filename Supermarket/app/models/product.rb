class Product < ApplicationRecord
  has_attached_file :image, styles: { medium: "200x200#" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
  searchkick
  belongs_to :category
  has_many :order_items
end
