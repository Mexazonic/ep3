Rails.application.routes.draw do
  resources :order_items
  resource :carts, only: [:show]

       root to: 'pages#home'
  devise_for :users
  resources :products do
    collection do
      get 'search'
    end
  end

  #root 'products#index'
end
